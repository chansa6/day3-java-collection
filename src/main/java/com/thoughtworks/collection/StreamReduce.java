package com.thoughtworks.collection;

import java.util.List;
import java.util.Optional;

public class StreamReduce {

    public int getLastOdd(List<Integer> numbers) {
//        return numbers.stream().filter(number -> number % 2 != 0).reduce((previous, current) -> current).orElse(null);
        return numbers.stream().reduce(0, (num1, num2) -> num2 % 2 == 0 ? num1 : num2);
    }

    public String getLongest(List<String> words) {
        return words.stream().reduce((word1, word2) -> word1.length() >= word2.length() ? word1 : word2).orElse("");
    }

    public int getTotalLength(List<String> words) {
        return words.stream().mapToInt(String::length).reduce(0, Integer::sum);
    }
}
